<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://use.typekit.net/fjd5jrj.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<nav>
		<div class="wrapper">


			<header>
				<div class="logo">
					<a href="<?php echo site_url('/'); ?>">
						<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="hamburger">
					<a href="#"></a>
				</div>
			</header>

			<div class="subnavs">
				<div class="subnavs-wrapper">
					
					<div class="coaching subnav">
						<div class="headline">
							<h4>Coaching</h4>
						</div>

						<div class="links">
							<?php if(have_rows('coaching_nav', 'options')): while(have_rows('coaching_nav', 'options')): the_row(); ?>
							    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
							<?php endwhile; endif; ?>
						</div>
					</div>

					<div class="college-season subnav">
						<div class="headline">
							<h4>College Season</h4>
						</div>
						
						<div class="links">
							<?php if(have_rows('college_season_nav', 'options')): while(have_rows('college_season_nav', 'options')): the_row(); ?>
							    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
							<?php endwhile; endif; ?>
						</div>
					</div>

				</div>
			</div>

		</div>
	</nav>