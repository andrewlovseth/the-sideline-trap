<?php

/*

	Template Name: Tournament Schedule

*/

get_header(); ?>

	<section id="tournament-schedule">
		<div class="wrapper">

			<article>

				<?php
					$args = array(
						'post_type' => 'tournaments',
						'posts_per_page' => 50
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


					<?php
						$first_place = get_field('1st_place');
						$second_place = get_field('2nd_place');
						$semifinalists = get_field('semifinalists');
					?>

					<div class="tournament">
						<div class="info">
							<h3><?php the_title(); ?></h3>
							<p><?php the_field('location'); ?> &middot; <?php $date = get_field('date', false, false); $date = new DateTime($date); echo $date->format('F j, Y'); ?></p>
						</div>

						<table class="results">
							<thead>
								<tr>
									<th class="first-place team">1st Place</th>
									<th class="second-place team">2nd Place</th>
									<th class="semifinalists team">Semifinals</th>
									<th class="results">Full Results</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td class="first-place team">
										<span class="label">1st Place</span>
										<?php if( $first_place ): ?>
										    <?php foreach( $first_place as $first_place_team): ?>
										    	<span class="team-name">
										    		<?php if(get_field('profile_page', $first_place_team->ID) == true): ?>
										    		<a href="<?php echo get_permalink($first_place_team->ID); ?>"><?php echo get_the_title($first_place_team->ID); ?></a>
										    		<?php else: ?>
										    			<?php echo get_the_title($first_place_team->ID); ?>
										    		<?php endif; ?>
										    	</span>
										    <?php endforeach; ?>
										<?php endif;?>
									</td>

									<td class="second-place team">
										<span class="label">2nd Place</span>
										<?php if( $second_place ): ?>
										    <?php foreach( $second_place as $second_place_team): ?>
										    	<span class="team-name"><?php echo get_the_title($second_place_team->ID); ?></span>
										    <?php endforeach; ?>
										<?php endif;?>
									</td>

									<td class="semifinalists team">
										<span class="label">Semifinalists</span>
										<?php if( $semifinalists ): ?>
										    <?php foreach( $semifinalists as $semifinalists_teams): ?>
										    	<span class="team-name"><?php echo get_the_title($semifinalists_teams->ID); ?></span>
										    <?php endforeach; ?>
										<?php endif;?>
									</td>

									<td class="results">
										<span class="label">Results</span>
										<a href="<?php the_field('score_reporter_link'); ?>" class="btn dark" rel="external">Score Reporter</a>
									</td>
								</tr>
							</tbody>
						</table>

					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</article>

		</div>
	</section>

<?php get_footer(); ?>