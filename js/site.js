$(document).ready(function() {


	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	// Smooth Scroll
	$('a.smooth').smoothScroll();


	// Hamburger Menu
	$('.hamburger a').click(function() {
		$(this).toggleClass('active');
		$('body').toggleClass('mobile-nav-open');
		$('.subnavs').slideToggle(200);


		return false;
	});


	$('nav .links a').click( function() {
		return false;
	});

});