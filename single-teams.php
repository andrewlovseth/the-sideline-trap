<?php get_header(); ?>

	<section id="team-profile">
		<div class="wrapper">

				<div class="basic-info">
					<div class="headline">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>	


			<article>
			
				<div class="results">		

					<?php

						// filter
						function my_posts_where( $where ) {
							
							$where = str_replace("meta_key = 'results_$", "meta_key LIKE 'results_%", $where);

							return $where;
						}

						add_filter('posts_where', 'my_posts_where');
						$team = get_the_ID();

						$args = array(
							'post_type' => 'tournaments',
							'posts_per_page' => 50,

							'meta_query'	=> array(
								'relation'		=> 'OR',
								array(
									'key'		=> 'results_$_team_1',
									'compare'	=> 'LIKE',
									'value'		=> '"' . $team . '"',
								),
								array(
									'key'		=> 'results_$_team_2',
									'compare'	=> 'LIKE',
									'value'		=> '"' . $team . '"',
								)
							)

						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<div class="tournament">
							<div class="info">
								<h3><?php the_title(); ?></h3>
								<p><?php the_field('location'); ?> &middot; <?php $date = get_field('date', false, false); $date = new DateTime($date); echo $date->format('F j, Y'); ?></p>

							</div>

							<div class="games">

								<?php if(have_rows('results')): while(have_rows('results')): the_row(); ?>

									<?php
										$team_1 = get_sub_field('team_1');
										$team_1_id = $team_1[0]->ID;
										$team_2 = get_sub_field('team_2');
										$team_2_id = $team_2[0]->ID;
									?>
								 
								 	<?php if($team_1_id == $team || $team_2_id == $team): ?>
					
									    <div class="game">
										    <div class="game-type">
										    	<span><?php the_sub_field('game_type'); ?></span>
										    </div>

									        <div class="team-1 team">

												<?php if( $team_1 ): ?>
												    <?php foreach( $team_1 as $team_1_name): ?>
												    	<span class="team-name">
												    		<?php if(get_field('profile_page', $team_1_name->ID) == true && $team_1_id != $team): ?>
												    			<a href="<?php echo get_permalink($team_1_name->ID); ?>"><?php echo get_the_title($team_1_name->ID); ?></a>
												    		<?php else: ?>
												    			<?php echo get_the_title($team_1_name->ID); ?>
												    		<?php endif; ?>
												    	</span>
												    <?php endforeach; ?>
												<?php endif;?>
									        </div>

									        <?php
									       		$team_1_score = get_sub_field('team_1_score');
									       		$team_2_score = get_sub_field('team_2_score');

									        	if($team_1_score > $team_2_score) {
									        		$winner = 'winner-team-1';
									        	} elseif($team_2_score > $team_1_score) {
									        		$winner = 'winner-team-2';
									        	}
									       ?>

									        <div class="score <?php echo $winner; ?>">
						        				<span class="points points-team-1 <?php if($team_1_id == $team):?>current-team<?php endif; ?>">
									    	    	<?php echo $team_1_score; ?>
									    	    </span>

									    	    <span class="dash">&ndash;</span>

									    	    <span class="points points-team-2 <?php if($team_2_id == $team):?>current-team<?php endif; ?>">
									    	    	<?php echo $team_2_score; ?>
									   	    	</span>
									        </div>

			

									        <div class="team-2 team">

												<?php if( $team_2 ): ?>
												    <?php foreach( $team_2 as $team_2_name): ?>
												    	<span class="team-name">
												    		<?php if(get_field('profile_page', $team_2_name->ID) == true && $team_2_id != $team): ?>
												    			<a href="<?php echo get_permalink($team_2_name->ID); ?>"><?php echo get_the_title($team_2_name->ID); ?></a>
												    		<?php else: ?>
												    			<?php echo get_the_title($team_2_name->ID); ?>
												    		<?php endif; ?>
												    	</span>
												    <?php endforeach; ?>
												<?php endif;?>
									        </div>


									    </div>

									<?php endif; ?>

								<?php endwhile; endif; ?>
							</div>

						</div>

					<?php endwhile; endif; wp_reset_postdata(); ?>

				</div>

			</article>

			<aside>
				<div class="roster">
					
				</div>
			</aside>
		




		</div>
	</section>

<?php get_footer(); ?>