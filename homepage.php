<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="about">
		<div class="wrapper">
			
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>