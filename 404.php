<?php get_header(); ?>

	<section id="page-not-found">
		<div class="wrapper">

			<h1>404 - Page Not Found</h1>
			<p>The page you are searching for cannot be found.</p>

		</div>
	</section>
	   	
<?php get_footer(); ?>